# Security Design Pattern Schema Repository

# Table of Contents
- [Description](#description)
- [JSON and JSON schemas](#json-and-json-schemas)
- [Repository documentation](#repository-documentation)
  - [Temporary schema urls ](#temporary-schema-urls)
  - [How to use](#how-to-use)
  - [Currently supported diagram types](#currently-supported-diagram-types)
  - [Soon supported diagram types](#soon-supported-diagram-types)
- [Structure schema documentation](#structure-schema-documentation)
  - [Security Solution Pattern Schema](#security-solution-pattern-schema)
  - [Security Design Pattern Schema](#security-design-pattern-schema)
- [Modeling language documentation](#modeling-language-documentation)
  - [UML 2 Class Diagram v1.0.0](#uml-2-class-diagram-v100)
  - [UML 2 Class Diagram v2.0.0](#uml-2-class-diagram-v200)
  - [UML 2 Sequence Diagram](#uml-2-sequence-diagram)
  - [ER Diagram](#er-diagram)
- [Examples](#examples)

# Description
This is the documentation of the Security Design Pattern (SDP) data structures. Please read the documentation carefully and refer back to it in case you don't understand certain parts of an SDP.

# JSON and JSON schemas
Security solution patterns, security design patterns and each supported modeling language, such as uml class diagrams, are defined by a `.schema.json` file defined in JSON schema version `2020-12`. 

A JSON file is a declarative definition of an object, that can be used for instance in JavaScript and be transfered just like a string between APIs. The following example defines a JSON object with a name, an address and hobbies:

```json 
{ 
    "name": "Foo Bar",
    "address": {
        "city": "Aachen",
        "plz": 52062,
        "street": "Templergraben 55"
    },
    "hobbies": ["computer science", "programming", "doing geeky stuff"]
}
```

JSON schemas are a way to describe the structure and data types of JSON (JavaScript Object Notation) data. It provides a standardized format for validating the structure and content of JSON documents. You can consider it the same way how classes and objects work in object oriented programming. In essence, a JSON schema acts like a blueprint that defines:

**Structure**: It specifies what fields (keys) can exist in the JSON object and whether they are required or optional.

**Data types**: It specifies the type of data (string, number, boolean, array, object, etc.) that can be stored in each field.

**Validation rules**: It specifies constraints and validation rules for the data, such as minimum and maximum values for numbers, regular expressions for strings, and so on.

Thus, if you know a schema, you can build JSON files according to the schema and use a JSON validator (for instance here: https://www.jsonschemavalidator.net/) that can validate the built JSON file against the JSON schema. If the schema is correct and the JSON object validates against the schema, then you know that the JSON is syntactically correct, too.

For more info refer to: https://json-schema.org/

# Repository documentation
## Temporary schema urls
The schemas are currently hosted under the following links. Use these to validate concrete JSON objects for syntactical validaty.

- class-diagram schema: https://api.jsonserve.com/MJuG4y
- class-diagram v2 schema: https://api.jsonserve.com/OhL80X
- sequence-diagram schema: https://api.jsonserve.com/_tSHby
- component-diagram schema: https://api.jsonserve.com/hFavDy
- er-diagram schema: https://api.jsonserve.com/Zquyiq
- security-design-pattern schema: https://api.jsonserve.com/ALphsW
- security-solution-pattern schema: https://api.jsonserve.com/pVO7v4
## How to use

This repository defines several JSON schema files that can be used to generate and verify security solution patterns and security design patterns in JSON.
The schema files follow a tree like structure, where the root is a security solution pattern, defined in `/structure/security-solution-pattern.schema.json`. This defines the JSON schema of the most abstract description of how to solve certain security requirements.

Then, for each security solution pattern, you can have 1 or more security design patterns, defined in `/structure/security-design-pattern.schema.json`. Each security design pattern defines certain models that describe the structural, dynamical and data aspects of how to implement the security solution pattern.

Each model - refered to as a diagram_type - is a leaf of this tree like structure as these are JSON schemas that define how to create concrete models, such as UML class diagrams, in this JSON DSL. 

Hence it is, for example:
```graphql
├── security solution pattern
│   ├── 1. security design pattern
│   │   ├── uml class diagram
│   │   ├── uml sequence diagram
│   │   ├── entity relationship diagram
│   ├── 2. security design pattern
│   │   ├── uml class diagram
│   │   ├── uml sequence diagram
│   ├── 3. security design pattern
│   │   ├── ...
│   ├── ...
```

The schemas are currently hosted over a temporay json file server. This will soon be removed to a self hosted file share.

## Currently supported diagram types

- UML class diagram
- UML class diagram v2
- UML sequence diagram
- Entity Relationship diagram

## Soon supported diagram types

- UML component diagram

# Structure schema documentation

## Security Solution Pattern Schema
This schema defines the most abstract entity of security solutions. They define all generic data on how to solve a certain security requirement.
The schema is defined as follows:

### Schema documentation

**Required**: "name", "type", "intent", "design_patterns"
```json
{
    "name": "Name of the solution pattern",
    "acronym": "Optional acronym of the solution pattern",
    "type": "Description of the solution pattern",
    "intent": "Intent of the solution pattern. Contains information on how, why and when to use it",
    "design_patterns": "Array of security design patterns"
}
```

## Security Design Pattern Schema
This schema defines a more concrete solution for a security solution pattern. Objects of this type define all information required to create architectural models. The schema is defined as follows:

### Schema doc
**Required**: "name"



```json
{
    "name": "Name of the security solution pattern",
    "description": "Optional acronym of the solution pattern",
    "structure": "Array of one or more structural diagrams such as UML class or component diagrams",
    "dynamics": "Array of one or more structural diagrams such as UML sequence diagrams",
    "data": "Array of one or more structural diagrams such as entity-relationship diagrams"
}
```

*Note 1*: Concrete diagram types are not required as there might be patterns without, for instance, any structural diagrams. In a later version a validator will added to this constraint. However, in the current version a security design pattern can in fact not contain any diagram information. Consider this case as invalid for now.

*Note 2*: The structure, dynamics and data currently all contain a reference to UML class diagrams. This is of course not final, but is only meant as a placeholder so that you can see how the schema is built up. The links will be updated when the JSON schemas for the other diagram types are published.

# Modeling language documentation

## UML 2 Class Diagram v1.0.0

### Language specification
```json
{
    "diagram_metadata": {
        "diagram_type": "class",
        "version": "1.0.0"
    }
}
```

### Diagram schema
**Required**: "name", "classes", "associations"
```json
{
    "name": "Name of the diagram",
    "discription": "Description of the diagram",
    "classes": "Array of class schemas",
    "associations": "Array of associations schemas"
}
```

#### Class schema
**Required**: "id", "name"
```json
{
    "id": "Unique identified of the class in the diagram",
    "name": "Name of the class",
    "attributes": "Array of attribute schemas used in the class",
    "methods": "Array of method schemas used in the class"
}
```

##### Attribute schema
**Required**: "name", "type"
```json
{
    "name": "Name of the attribute",
    "type": "Data type of the attribute",
    "visibility": "Visibility of the attribute. Any of: 'public', 'private', 'protected'"
}
```


##### Method schema
**Required**: "name", "returnType"
```json
{
    "name": "Name of the method",
    "returnType": "Return data type of the method",
    "visibility": "Visibility of the attribute. Any of: 'public', 'private', 'protected'",
    "parameters": "Array of parameter schemas"
}
```

##### Parameter schema
**Required**: "name", "type"
```json
{
    "name": "Name of the attribute",
    "type": "Data type of the attribute"
}
```

#### Associations schema
**Required**: "id", "name", "type", "source", "target"
**Conditionally required**: if "ternary_relationship" is true: "ternary_element", "ternary_multiplicity"

```json
{
    "id": "Unique identified of the association in the diagram",
    "name": "The name of the association",
    "type": "The type of the association. Can by any of: 'association', 'navigable association', 'inheritance', 'realization', 'dependency', 'aggregation', 'composition'",
    "source": "Class id of the source of the association",
    "target": "Class id of the target of the association",
    "source_multiplicity": "The multiplicity of the source element of the association. Can by any of: '1', '0..1', '0..*', '1..*', '*'",
    "target_multiplicity": "The multiplicity of the target element of the association. Can by any of: '1', '0..1', '0..*', '1..*', '*'",
    "ternary_relationship": "Boolean. True if association has a third element",
    "ternary": "Required only if ternary_relationship is true. Class id of the third element of the association",
    "ternary_multiplicity": "Required only if ternary_relationship is true. The multiplicity of the third element of the association. Can be any of: '1', '0..1', '0..*', '1..*', '*'""
}
```

## UML 2 Class Diagram v2.0.0

### Language specification
```json
{
    "diagram_metadata": {
        "diagram_type": "class",
        "version": "2.0.0"
    }
}
```

### Diagram schema
**Required**: "name", "scope", "classes", "associations"
```json
{
    "name": "Name of the diagram",
    "discription": "Description of the diagram",
    "scope": "Defines whether the class diagram should be strictly interpreted according the UML class metamodel, or loose. The loose case is often used to model logical views with UML class diagrams, that do not required visibility of attributes or methods. Can be any of: 'loose', 'strict'",
    "classes": "Array of class schemas",
    "associations": "Array of associations schemas"
}
```

#### Class schema
**Required**: "id", "name"
```json
{
    "id": "Unique identified of the class in the diagram",
    "name": "Name of the class",
    "classifier": "Defines a classification of a class. Can be any of 'abstract', 'interface', 'enum'",
    "stereotype": "Custom stereotype. If both classifier and stereotype are set, then stereotype should be ignored.",
    "super": "Class id of another class, if this class is a specialization or realization of another class",
    "attributes": "Array of attribute schemas used in the class",
    "methods": "Array of method schemas used in the class"
}
```

##### Attribute schema
**Required**: "name", "type"
```json
{
    "name": "Name of the attribute",
    "type": "Data type of the attribute",
    "visibility": "Visibility of the attribute. Any of: 'public', 'private', 'protected'"
}
```


##### Method schema
**Required**: "name", "type"
```json
{
    "name": "Name of the method",
    "type": "Return data type of the method",
    "visibility": "Visibility of the attribute. Any of: 'public', 'private', 'protected'",
    "parameters": "Array of parameter schemas"
}
```

##### Parameter schema
**Required**: "name", "type"
```json
{
    "name": "Name of the attribute",
    "type": "Data type of the attribute"
}
```

#### Associations schema
**Required**: "id", "name", "type", "source", "target"

```json
{
    "id": "Unique identity of the association in the diagram",
    "name": "The name of the association",
    "type": "The type of the association. Can by any of: 'association', 'aggregation', 'composition'",
    "entities": "A list of entity schemas"
}
```

#### Entities schema
**Required**: "id", "role"
**Conditionally required**: if "association.type" is "association": "navigability"

```json
{
    "id": "Unique identity of the association in the diagram",
    "multiplicity": "The multiplicity of the entity in the association. Can be any of: '1', '0..1', '0..*', '1..*",
    "role": "The role of the entity in the association. Can by any of: 'source', 'target', 'other'",
    "navigability": "Defines the navigability for association types. Can be any of: 'navigable', 'not_navigable', 'unspecified'"
}
```
## UML 2 Component Diagram

### Language specification
```json
{
    "diagram_metadata": {
        "diagram_type": "component",
        "version": "1.0.0"
    }
}
```

### Diagram schema

**Required**: "name", "components", "interfaces"
```json
    "name": "Name of the diagram",
    "description": "Description of the diagram",
    "components": "Array of component schemas",
    "artifacts": "Array of artifact schemas",
    "classes": "Array of class schemas",
    "interfaces": "Array of interface schemas",
    "connectors": "Array of connector schemas",
    "relationships": "Array of relationship schemas"
```

#### Component schema

**Required**: "id", "name", "ports"
```json
    "id": "The unique identifier of the component",
    "name": "The name of the component",
    "stereotype": "Optional stereotype of the component, for instance 'service', or 'database'",
    "ports": "Array of port schemas"
```

##### Port schema

**Required**: "id",
```json
    "id": "The unique identifier of the port",
    "requried_interfaces": "Array of interface ids of all required interfaces of the port",
    "provided_interfaces": "Array of interface ids of all provided interfaces of the port"
```

#### Artifact schema

**Required**: "id", "name", "stereotype"
```json
    "id": "The unique identifier of the artifact",
    "name": "The name of the artifact",
    "stereotype": "The type of artifact. Can be any of: 'artifact', 'specification'"
```

#### Class schema

**Required**: "id", "name"
```json
    "id": "The unique identifier of the class",
    "name": "The name of the class",
```

#### Interface schema

**Required**: "id"
```json
    "id": "The unique identifier of the interface"
```

#### Connector schema

**Required**: "id", "source", "target"
```json
    "id": "The unique identifier of the connector",
    "source": "The id of the source port",
    "target": "The id of the target port"
```

#### Relationship schema
**Required**: "id", "type", "source", "target"
```json
    "id": "The unique identifier of the relationship",
    "type": "The type of the relationship. Can by any of: 'use', 'manifest', 'dependency', 'delegation'",
    "source": "The id of the source artifact, class, component, port or interface"
    "target": "The id of the target artifact, class, component, port or interface"
```
❗Note: JSON schemas cannot be more strict in checking the right types of the source and target of a relationship. Hence, applications using this schema to create component diagrams should implement the following internal checks:
- Relationships of type 'manifest' must have an artifact as a source and component as target.
- Relationships of type 'delegation' must have an interface as source and connector as target, or a connector as source and an interface as target.

## UML 2 Sequence Diagram

### Language specification
```json
{
    "diagram_metadata": {
        "diagram_type": "class",
        "version": "1.0.0"
    }
}
```

### Diagram schema

**Required**: "name", "participants", "messages"
```json
    "name": "Name of the diagram",
    "discription": "Description of the diagram",
    "participants": "Array of participant schemas",
    "messages": "Array of message schemas",
    "fragments": "Array of fragment schemas"
}
```

#### Participant schema

**Required**: "id", "name", "type"
```json
{
  "id": "The id of the participant. Used to uniquely identify sender and receiver of a message",
  "name": "The name of the participant",
  "type": "The type of the participant. Can by any of: 'actor', 'object'"
}
```

#### Message schema

**Required**: "name", "sender", "receiver", "type", "sequence_number"

```json
{
  "name": "The name of the message.",
  "sender": "The participant id of the sender of the message.",
  "receiver": "The participant id of the receiver of the message.",
  "type": "The type of the message. Can be any of: 'synchronous', 'asynchronous', 'reply'",
  "sequence_number": "The order of the message in the sequence. Type: integer"
}
```

#### Fragment schema

**Required**: "type"

```json
{
  "type": "The type of interaction fragment. Can by any of: 'alt', 'opt', 'loop', 'par', 'region'",
  "fragments": "Nested fragments within this fragment. Follows the fragment schema"
}
```

###### Fragments of type: opt, loop, region, sd
**Required**: "sequence_numbers"

```json
{
  "description": "The description giving a brief explanation of what this fragment is all about",
  "sequence_numbers": "List of sequence numbers of type integer that are included in this frame",
}
```

###### Fragments of type: alt, opt
**Required**: "blocks"

```json
{
  "blocks": "An array of block objects that are defined for this alt or opt fragment.",
}
```
###### Blocks schema
**Required**: "condition", "sequence_numbers"
```json
{
    "condition": "The condition to be true for this block",
    "sequence_numbers": "List of sequence numbers of type integer that are included in this block",
}
```


## ER Diagram

### Language specification
```json
{
    "diagram_type": "er"
}
```

### Diagram schema

**Required**: "name", "entities", "relationships"
```json
{
    "name": "Name of the diagram",
    "discription": "Description of the diagram",
    "entities": "List of entity schemas",
    "relationships": "List of relationship schemas",
}
```

#### Entity schema

**Required**: "id", "name", "attributes"

```json
{
    "id": "The id of the entity. Used to uniquely identify the entity",
    "name": "The name of the entity",
    "attributes": "List of attribute schemas"
}
```


#### Relationship schema

**Required**: "id", "name", "entities"

```json
{
    "id": "The id of the relationship. Used to uniquely identify the relationship",
    "name": "The name of the relationship",
    "entities": "List of involved entity schemas. Must be minimum 2",
    "attributes": "List of relationship attribute schemas"
}
```
#### Involved Entity schema

**Required**: "id", "cardinality"
```json
{
    "id": "The id of the entity involved in the relationship.",
    "cardinality": "The cardinality of the entity in the relationship. Can by any of: '1', '0..1', '1..*', '0..*'"
}
```

#### Relationship Attribute schema

**Required**: "id", "cardinality"
```json
{
    "id": "The id of the entity involved in the relationship.",
    "name": "The name of the attribute",
    "type": "The data type of the attribute"
}
```

#### Attribute schema

**Required**: "id", "name", "type"
**Conditionally required**: If "composite" is true, "component_attributes" must not be null. If "derived" is true, "derived_from" must not be null.

```json
{
    "id": "The id of the attribute. Used to uniquely identify the attribute",
    "name": "The name of the attribute",
    "type": "The data type of the attribute",
    "enum": "If this attribute defines an enum, defines a list of enum values",
    "primary_key": "Whether the attribute is a primary key",
    "foreign_key": "Whether the attribute is a foreign key",
    "multivalued": "Whether the attribute is multivalued",
    "composite": "Whether the attribute is composite",
    "component_attributes": "If composite is true, defines the list of attribute ids that form this composite attribute",
    "derived": "Whether the attribute is derived",
    "derived_from": "If derived is true, defines the list of attribute ids that this attribute is derived from"
}
```

# Examples
## Security solution pattern
```json
{
    "name": "Role-based Access Control",
    "acronym": "RBAC",
    "type": "Authorization",
    "intent": "The Role-based Access Control pattern describes how to assign rights based on the functions or tasks of users in an environment in which control of access to computing resources is required.",
    "variations": [
        {
            "name": "Variant 1: Generic RBAC",
            "description": "This solution pattern describes a generic RBAC solution that can be used and adapted in many different scenrarios and circumstances",
            "structure": [
                {
                    "diagram_type": "class",
                    "name": "",
                    "discription": "The User and Role classes describe registered users and their predefined roles respectively. Users are assigned to roles; roles are given rights according to their functions. The association class Right defines the access types that a user within a role is authorized to apply to the protection object. The Role class attempts to access a resource (Protection Object) in some way. The ProtectionObject represents the resource to be protected. The association between the Role and the ProtectionObject defines an authorization, from which the pattern gets its name. The association class Right describes the access type (for example, read, write) the Role is allowed to perform on the corresponding ProtectionObject. Through this class one can check the rights that a subject has on some object, or who is allowed to access a given object.",
                    "classes": [
                        {
                            "id": "1",
                            "name": "User",
                            "attributes": [
                                {
                                    "name": "id",
                                    "type": "string"
                                },
                                {
                                    "name": "userName",
                                    "type": "string"
                                }
                            ]
                        },
                        {
                            "id": "2",
                            "name": "Role",
                            "attributes": [
                                {
                                    "name": "id",
                                    "type": "string"
                                },
                                {
                                    "name": "roleName",
                                    "type": "string"
                                }
                            ]
                        },
                        {
                            "id": "3",
                            "name": "Right",
                            "attributes": [
                                {
                                    "name": "accessType",
                                    "type": "string",
                                    "enum": [
                                        "canAdd",
                                        "canView",
                                        "canEdit",
                                        "canDelete"
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "4",
                            "name": "Resource",
                            "attributes": [
                                {
                                    "name": "id",
                                    "type": "string"
                                }
                            ]
                        }
                    ],
                    "relationships": [
                        {
                            "id": "1",
                            "name": "memberOf",
                            "type": "association",
                            "source": "1",
                            "target": "2",
                            "source_multiplicity": "1..*",
                            "target_multiplicity": "1..*"
                        },
                        {
                            "id": "2",
                            "name": "memberOf",
                            "type": "association",
                            "ternary_relationship": true,
                            "source": "2",
                            "target": "4",
                            "ternary": "3",
                            "source_multiplicity": "1..*",
                            "target_multiplicity": "1..*",
                            "ternary_multiplicity": "1"
                        }
                    ]
                }
            ],
            "dynamics": [],
            "data": []
        }
    ]
}
```

## UML class diagram
```json
{
        "classes": [
          {
            "id": "1",
            "name": "Person",
            "attributes": [
              {
                "name": "name",
                "type": "string",
                "visibility": "public"
              },
              {
                "name": "age",
                "type": "integer",
                "visibility": "private"
              }
            ],
            "methods": [
              {
                "name": "getName",
                "returnType": "string",
                "parameters": [],
                "visibility": "public"
              },
              {
                "name": "getAge",
                "returnType": "integer",
                "parameters": [],
                "visibility": "protected"
              }
            ]
          },
          {
            "id": "2",
            "name": "Company",
            "attributes": [
              {
                "name": "companyName",
                "type": "string",
                "visibility": "public"
              },
              {
                "name": "location",
                "type": "string",
                "visibility": "protected"
              }
            ],
            "methods": [
              {
                "name": "getCompanyName",
                "returnType": "string",
                "parameters": [],
                "visibility": "public"
              },
              {
                "name": "getLocation",
                "returnType": "string",
                "parameters": [],
                "visibility": "private"
              }
            ]
          }
        ],
        "relationships": [
          {
            "id": "1",
            "name": "employment",
            "type": "navigable association",
            "source": "1",
            "target": "2",
            "multiplicity": "1..*"
          },
          {
            "id": "2",
            "name": "inheritance",
            "type": "inheritance",
            "source": "1",
            "target": "2"
          },
          {
            "id": "3",
            "name": "realization",
            "type": "realization",
            "source": "1",
            "target": "2"
          },
          {
            "id": "4",
            "name": "dependency",
            "type": "dependency",
            "source": "1",
            "target": "2"
          },
          {
            "id": "5",
            "name": "aggregation",
            "type": "aggregation",
            "source": "1",
            "target": "2"
          },
          {
            "id": "6",
            "name": "composition",
            "type": "composition",
            "source": "1",
            "target": "2"
          }
        ]
      }
```

## UML 2 Sequence Diagram
```json
{
    "diagram_type": "sequence",
    "name": "Sequence Diagram Scenario Name",
    "description": "Some description of this scenario",
    "participants": [
        {
            "id": "1",
            "name": "User",
            "type": "actor"
        },
        {
            "id": "2",
            "name": "Endpoint",
            "type": "object"
        },
        {
            "id": "3",
            "name": "UserEndpoint",
            "type": "object"
        },
        {
            "id": "4",
            "name": "ResourceEndpoint",
            "type": "object"
        },
        {
            "id": "5",
            "name": "RolesDB",
            "type": "object"
        },
        {
            "id": "6",
            "name": "ResouceDB",
            "type": "object"
        },
        {
            "id": "7",
            "name": "RightsDB",
            "type": "object"
        }
    ],
    "messages": [
        {
            "name": "read restricted resource",
            "sender": "1",
            "receiver": "2",
            "type": "synchronous",
            "sequence_number": 1
        },
        {
            "name": "check authentication",
            "sender": "2",
            "receiver": "3",
            "type": "synchronous",
            "sequence_number": 2
        },
        {
            "name": "user not authenticated",
            "sender": "3",
            "receiver": "2",
            "type": "reply",
            "sequence_number": 3
        },
        {
            "name": "deny access",
            "sender": "2",
            "receiver": "1",
            "type": "reply",
            "sequence_number": 4
        },
        {
            "name": "return user object",
            "sender": "3",
            "receiver": "2",
            "type": "reply",
            "sequence_number": 5
        },
        {
            "name": "request access to resource for user object",
            "sender": "2",
            "receiver": "4",
            "type": "synchronous",
            "sequence_number": 6
        },
        {
            "name": "get use roles",
            "sender": "4",
            "receiver": "5",
            "type": "synchronous",
            "sequence_number": 7
        },
        {
            "name": "return user roles object",
            "sender": "5",
            "receiver": "4",
            "type": "reply",
            "sequence_number": 8
        },
        {
            "name": "get resource",
            "sender": "4",
            "receiver": "6",
            "type": "synchronous",
            "sequence_number": 9
        },
        {
            "name": "return resource object",
            "sender": "6",
            "receiver": "4",
            "type": "reply",
            "sequence_number": 10
        },
        {
            "name": "get user resource rights",
            "sender": "4",
            "receiver": "7",
            "type": "synchronous",
            "sequence_number": 11
        },
        {
            "name": "return user resource rights object",
            "sender": "7",
            "receiver": "4",
            "type": "reply",
            "sequence_number": 12
        },
        {
            "name": "check if user resource rights includes 'canView'",
            "sender": "4",
            "receiver": "4",
            "type": "synchronous",
            "sequence_number": 13
        },
        {
            "name": "throw unauthorized exception",
            "sender": "4",
            "receiver": "2",
            "type": "reply",
            "sequence_number": 14
        },
        {
            "name": "deny access",
            "sender": "2",
            "receiver": "1",
            "type": "reply",
            "sequence_number": 15
        },
        {
            "name": "return resource",
            "sender": "4",
            "receiver": "2",
            "type": "reply",
            "sequence_number": 16
        },
        {
            "name": "show resource",
            "sender": "2",
            "receiver": "1",
            "type": "reply",
            "sequence_number": 17
        }
    ],
    "fragments": [
        {
            "type": "alt",
            "blocks": [
                {
                    "condition": "user unauthenticated",
                    "sequence_numbers": [
                        3,
                        4
                    ]
                },
                {
                    "condition": "user authenticated",
                    "sequence_numbers": [
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15,
                        16,
                        17
                    ]
                }
            ],
            "fragments": [
                {
                    "type": "alt",
                    "blocks": [
                        {
                            "condition": "'canView' not included",
                            "sequence_numbers": [
                                14,
                                15
                            ]
                        },
                        {
                            "condition": "'canView' is included",
                            "sequence_numbers": [
                                16,
                                17
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}
```

## ER Diagram
```json
{
                "diagram_type": "er",
                "name": "User and Role Access Control",
                "description": "ER diagram based on the UML class diagram describing registered users, their predefined roles, the resources they access, and the rights associated with those roles.",
                "entities": [
                    {
                        "id": "User",
                        "name": "User",
                        "attributes": [
                            {
                                "id": "user_id",
                                "name": "id",
                                "type": "string",
                                "primary_key": true,
                                "composite": false,
                                  "derived": false
                            },
                            {
                                "id": "user_userName",
                                "name": "userName",
                                "type": "string",
                                "composite": false,
                                  "derived": false
                            }
                        ]
                    },
                    {
                        "id": "Role",
                        "name": "Role",
                        "attributes": [
                            {
                                "id": "role_id",
                                "name": "id",
                                "type": "string",
                                "primary_key": true,
                                "composite": false,
                                  "derived": false
                            },
                            {
                                "id": "role_roleName",
                                "name": "roleName",
                                "type": "string",
                                "composite": false,
                                  "derived": false
                            }
                        ]
                    },
                    {
                        "id": "Resource",
                        "name": "Resource",
                        "attributes": [
                            {
                                "id": "resource_id",
                                "name": "id",
                                "type": "string",
                                "primary_key": true,
                                "composite": false,
                                  "derived": false
                            }
                        ]
                    },
                    {
                        "id": "Right",
                        "name": "Right",
                        "attributes": [
                            {
                                "id": "right_accessType",
                                "name": "accessType",
                                "type": "string",
                                "enum": [
                                    "canAdd",
                                    "canView",
                                    "canEdit",
                                    "canDelete"
                                ],
                                "composite": false,
                                  "derived": false
                            },
                            {
                                "id": "right_roleId",
                                "name": "role_id",
                                "type": "string",
                                "foreign_key": true,
                                "composite": false,
                                  "derived": false
                            },
                            {
                                "id": "right_resourceId",
                                "name": "resource_id",
                                "type": "string",
                                "foreign_key": true,
                                "composite": false,
                                  "derived": false
                            }
                        ]
                    }
                ],
                "relationships": [
                    {
                        "name": "has",
                        "entities": ["User", "Role"],
                        "cardinality": ["1..*", "1..*"]
                    },
                    {
                        "name": "is authorized for",
                        "entities": ["Role", "Resource", "Right"],
                        "cardinality": ["1..*", "1..*", "1"]
                    }
                ]
            }
```