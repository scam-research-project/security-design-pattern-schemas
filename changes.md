# Version 10.07.24
## UML Component diagram
1. An error was fixed that allowed compositions and aggregations to have a navigability, too.
2. A `stereotype: string` property was added to classes to describe custom stereotypes. If both classifier and stereotype are set, then stereotype should be ignored.

## UML Sequence diagram
1. Enforced `additionalProperties: false` for par and alt fragments for validation purposes.

# Version 05.07.24
## UML Component diagram
1. Interfaces and Connectors cannot have a name anymore.
2. Relationships can have a new type "delegation"
Please also check the important note for relationships in the README.md

# Version 26.06.24
1. A new attribute "enum" was added to `entity attributes`. This defines an enum to be an array of strings, where each strin represents a valid value of this enum.
2. Attributes of `relationships` can no longer have invalid properties, such as primary keys.

# Version 20.06.24 v2

## ER diagram
2. A new attribute "id" has been added to `relationships`.
1. The `relationship` object was changed. It defined 2 lists: "entities" and "cardinality". This approach was missing a uniquely identification which cardinality belonged on which side of the relationship/which entity. Thus, it was remodeled to the following structure:
```json
{
    "relationships": {
        "id": "string",
        "name": "string",
        "entities": [
            {
                "id": "The entity id to be referenced",
                "cardinality": "The cardinality of this entity"
            }
        ]
    }
}
```

# Version 20.06.24

## UML class diagram

1. The `"diagram_type": "string"` was changed to `"diagram_type": {"diagram_type": "string", "version": "string"}`. This allows you to implement different parsing strategies for different major language versions (1.x.y, 2.x.y, etc.) and allows you to pull changes of your SDP repository without your parsers to break. You must, however, update your business logic according to the changes (not getting the diagram_type from the json, but the diagram_metadata.diagram_type and version vice versa).
2. The biggest changes were in the associations. They allow n-ary associations now. For this every association holds a list of "entities" and each entity defines its own multiplicity, nevigation direction and its role in the association, i.e. whether it is the source, the target, or another role. 
3. The attribute `visibility` was added to attributes and methods. Possible values are: "public", "private", "protected", "package"
4. A new attribute `scope` has been added on diagram level. The current supported values are: "loose", "strict". If its loose, visibility of attributes and methods is not required (and should not represented that way). If its strict, visibility of attributes and methods is required (i.e. an attribute without a +, -, o has visibility level: package, unlike in the "loose" case, where it is simply not defined).
5. A new attribute `classifier` of type string has been added on class level. Its purpose is to identify classes as "abstract", "interface" or "enum".
6. The association types "specialization", "realization" and "dependency" were removed.
7. A new attribute `super` of type string was added on class level. If a class inherits another class or implements an interface, then the class id which this class inherits or implements as given in this attribute.
8. The attribute `returnType` for methods was renamed to `return_type` to not break common DSL design, which uses underscore instead of lowerCamelCase.
9. Multiplicity is no longer required for UML class associations

# Version 19.06.24

1. A json schema for ER diagrams was added.
2. All api urls have been updated.
3. The rbac-solution-pattern.json was extended by an ER diagram


# Version 15.06.24

## sequence_diagram.schema.json

1. The fragment of type `sd` added in last update was removed, as it just bloats nested fragments (every fragment would be a nested fragment of the sd root fragment). As sd fragments are special fragments that always enclose all participants and messages, the name of the sequence diagram can be used for an sd fragment in clients instead.
2. The `sequence_diagram.schema.json` was a wrong copy+paste and showed the actual rbac example. I replaced the example with the actual schema code again.

# Version 14.06.24

## sequence_diagram.schema.json

Changes were made to add internal blocks for "alt" and "par" fragments. 
1. sequence_numbers are now only required for fragments of type: "opt", "loop", and "region"
2. An additional fragment type has been added: `sd` which represents a wrapper for sequence diagram as a whole (sd = sequence diagram).
3. fragments of the types `opt, loop, region, sd` require a `sequence_number` array, i.e. which sequence_numbers are encapsulated by this fragment
4. fragments of the types `opt, loop, region, sd` **do not require** a `condition` anymore.
5. An optional `description` attribute has been added. A description shall be added to a fragment to give a brief explanation of what this fragment is all about. 
6. if a fragment type is of type "alt" or "par" is given, a `blocks` property must be provided. `blocks` is an array of objects with required properties `condition`, which is the description of the condition to be hold for this block, and `sequence_numbers`, which is an array of the sequence_numbers being encapsulated by this block.
7. fragments of type `alt` or `par` **do not have** a `description` attribute. The `condition` represents a kind of description for this kind.